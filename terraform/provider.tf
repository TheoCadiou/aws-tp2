# PROVIDER


provider "aws" {
  region  = var.region
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.13.0"
    }
  }
}
